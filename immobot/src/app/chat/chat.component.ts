import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss'],
})
export class ChatComponent implements OnInit {
  mensajes = [
    'Hola! Cesar Miranda',
    'De acuerdo con tu informacion, calificas a los departamentos mostrados en la parte derecha',
    'Comentame cual es el numero del departamento en el que estarias interesado',
  ];
  plano1 =
    'El apartamento 1 cuenta con un baño, cocina, cama para dos personas, un sofa mediano, un sofa largo y un patio';
  plano2 =
    'El apartamento 2 cuenta con un baño, cocina, cama para dos personas, un sofa mediano y un sofa largo';
  plano3 =
    'El apartamento 3 cuenta con un baño, cocina, cama para dos personas, dos sofa medianos, un sofa largo, un comedor con mesa y cuatro sillas ademas de un patio';
  preguntar: string;
  constructor(private router: Router) {}

  ngOnInit(): void {}

  preguntarClick() {
    if (this.preguntar == undefined || this.preguntar == null) return;
    if (this.preguntar.includes('1')) {
      this.mensajes.push(this.plano1);
      return;
    }
    if (this.preguntar.includes('2')) {
      this.mensajes.push(this.plano2);
      return;
    }
    if (this.preguntar.includes('3')) {
      this.mensajes.push(this.plano3);
      return;
    }
  }

  salir() {
    this.router.navigateByUrl('');
  }
}
